import React from 'react';
import { Helmet } from 'react-helmet';
import { PapperBlock } from 'enl-components';

class SamplePage extends React.Component {
    render() {
        const title = 'Enlite Prime. Blank Page';
        const description = 'Enlite Prime';
        return (
            <div>
                <Helmet>
                    <title>{title}</title>
                    <meta name="description" content={description} />
                    <meta property="og:title" content={title} />
                    <meta property="og:description" content={description} />
                    <meta property="twitter:title" content={title} />
                    <meta property="twitter:description" content={description} />
                </Helmet>
                <PapperBlock title="Blank Page" desc="Some text description">
                    Content
                </PapperBlock>
            </div>
        );
    }
}

export default SamplePage;